# docker-zammad

This is basically a clone of the official [Repository](https://github.com/zammad/zammad-docker-compose). The docker-compose-override.yml has some modifications to fit our environment and the env-vars are provided inside Portainer. Whenever the Official Repo changend, we will adopt these modifications inside this Repo.